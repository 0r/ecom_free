<?php
    //global variable used for hosting connection
    $dbusername = 'rmsu_ecomdb';
    $dbpassword = 'secretpassword';
    $dbserver = 'localhost';
    $dbname='rmsu_ecomdb';

    //run this on console if u using docker
    //docker run --name proecomdb  -e MYSQL_ROOT_PASSWORD=secretpassword -e MYSQL_USER=rmsu_ecomdb -e MYSQL_PASSWORD=secretpassword -e MYSQL_DATABASE=rmsu_ecomdb -p 3306:3306 -d mysql --default-authentication-plugin=mysql_native_password
    
    //hosting db connection
    $conn = mysqli_connect( "$dbserver", "$dbusername", "$dbpassword", "$dbname" );

    if( !$conn ) // == null if creation of connection object failed
    {
        // bypass on windows environment with xampp
        $conn = mysqli_connect( "$dbserver", "root", "", "$dbname" );
        // bypass on linux/windows env with docker
        if( !$conn ) $conn = mysqli_connect( "127.0.0.1", "$dbusername", "$dbpassword", "$dbname" ) 
        // report the error to the user, then exit program
        or die ("connection object not created: ".mysqli_error($conn));
    }

    if( mysqli_connect_errno() )  // returns false if no error occurred
    {
        // report the error to the user, then exit program
        die ("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
    }
