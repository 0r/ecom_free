create database ecomdb;
use ecomdb;
create table tbl_admin(
                        id_admin integer PRIMARY KEY auto_increment,
                        username varchar(50) not null,
                        password varchar(50) not null,
                        nama varchar(50) ,
                        email varchar(50)
);

create table tbl_kategori(
                           id_kategori integer PRIMARY KEY auto_increment,
                           nama_kategori varchar(50)
);

create table tbl_biaya_kirim(
                              id_biaya_kirim integer PRIMARY KEY auto_increment,
                              kota varchar(50) not null,
                              biaya integer not null
);

create table tbl_merek(
                        id_merek integer PRIMARY KEY auto_increment ,
                        nama_merek varchar(50) not null
);

create table tbl_member(
                         id_member integer PRIMARY KEY auto_increment ,
                         username varchar(50) not null unique,
                         password varchar(50) not null,
                         nama varchar(50) not null,
                         alamat varchar(50) not null,
                         email varchar(50) not null,
                         no_hp varchar(50) not null
);

create table tbl_produk(
                         id_produk integer PRIMARY KEY auto_increment,
                         id_kategori_produk integer,
                         id_merek integer references tbl_merek(id_merek),
                         nama_produk varchar(100) not null,
                         deskripsi text,
                         harga integer not null,
                         gambar varchar(100),
                         slide char(1),
                         rekomendasi char(1),
                         foreign key(id_kategori_produk) references tbl_kategori(id_kategori),
                         foreign key(id_merek) references tbl_merek(id_merek)
);

create table tbl_order(
                        id_order integer PRIMARY KEY auto_increment,
                        id_member integer references tbl_member(id_member),
                        status_order char(1) not null,
                        tgl_order date not null,
                        jam_order time not null,
                        foreign key(id_member) references tbl_member(id_member)
);


create table tbl_detail_order(
                               id_detail_order integer PRIMARY KEY auto_increment,
                               id_order integer references tbl_order(id_order),
                               id_produk integer references tbl_produk(id_produk),
                               jumlah integer not null,
                               harga integer not null,
                               foreign key(id_order) references tbl_order(id_order),
                               foreign key(id_produk) references tbl_produk(id_produk)
);
